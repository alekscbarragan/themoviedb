//
//  UIButton+Selection.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIButton {
    static func animateSelection(sender: UIButton) {
        sender.isUserInteractionEnabled = false
        
        let originalTransform = sender.transform
        let newTransform = originalTransform.scaledBy(x: 0.75, y: 0.75)
        let duration = 0.15
        
        UIView.transition(with: sender,
                          duration: duration,
                          options: .curveEaseInOut,
                          animations: { sender.isSelected = !sender.isSelected },
                          completion: nil)
        
        let animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.5) {
            sender.transform = newTransform
        }
        
        let finishedTransform = originalTransform.scaledBy(x: 1.25, y: 1.25)
        let finishedAniamtor = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.5) {
            sender.transform = finishedTransform
        }

        let initialStateAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.5) {
            sender.transform = originalTransform
            sender.isUserInteractionEnabled = true
        }
        
        finishedAniamtor.addCompletion { (_) in
            initialStateAnimator.startAnimation()
        }
        
        animator.addCompletion { (_) in
            finishedAniamtor.startAnimation()
        }
        
        animator.startAnimation()
    }
}
