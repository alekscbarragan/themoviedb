//
//  UIEdgeInsets+.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIEdgeInsets {
    static var all: (CGFloat) -> UIEdgeInsets = { value in
        let insets = UIEdgeInsets(top: value, left: value, bottom: value, right: value)
        return insets
    }
    
    static func insetAll(inset: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    static var label: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
}
