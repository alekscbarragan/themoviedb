//
//  UILabel+Create.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UILabel {
    static func create(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.defaultConfiguration()
        return label
    }
    
    func defaultConfiguration() {
        translatesAutoresizingMaskIntoConstraints = false
        textColor = .text
        font = UIFont.systemFont(ofSize: 15)
        numberOfLines = 5
        adjustsFontSizeToFitWidth = true
    }
}
