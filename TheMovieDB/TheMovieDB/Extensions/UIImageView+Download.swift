//
//  UIImageView+Download.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Kingfisher
import UIKit

extension UIImageView {
    func downloadImage(path: String?, completion: ((_ success: Bool) -> Void)? = nil) {
        guard let path = path else {
            print("❌ Could not download image because path is nil.")
            return
        }
        
        let baseUrl = Environment.imageBaseUrlString
        guard var url = URL(string: baseUrl) else {
            print("❌ Could not create URL with \(Environment.imageBaseUrlString)")
            return
        }
        
        url.appendPathComponent(path)
        
        kf.indicatorType = .activity
        kf.setImage(with: url) { (result) in
            switch result {
            case .success:
                completion?(true)
                
            case .failure(let error):
                print("❌ Could not download image. Error: \(error.localizedDescription)")
                completion?(false)
            }
        }
    }
    
    func downloadImage(withUrlString urlString: String?, completion: ((_ success: Bool) -> Void)? = nil) {
        guard let urlString = urlString else {
            print("❌ Could not download image because path is nil.")
            return
        }
        
        guard let url = URL(string: urlString) else {
            print("❌ Could not create URL with \(Environment.imageBaseUrlString)")
            return
        }
        
        kf.indicatorType = .activity
        kf.setImage(with: url) { (result) in
            switch result {
            case .success:
                completion?(true)
                
            case .failure(let error):
                print("❌ Could not download image. Error: \(error.localizedDescription)")
                completion?(false)
            }
        }
    }
}
