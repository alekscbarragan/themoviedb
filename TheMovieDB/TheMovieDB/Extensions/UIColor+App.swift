//
//  UIColor+App.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

extension UIColor {
    static var text: UIColor {
        if #available(iOS 13.0, *) {
            return .label
        } else {
            // Fallback on earlier version
            return .black
        }
    }
    
    static var background: UIColor {
        if #available(iOS 13.0, *) {
            return .systemBackground
        } else {
            return .white
        }
    }
    
    static var control: UIColor {
        if #available(iOS 13.0, *) {
            return .systemOrange
        } else {
            return .red
        }
    }
}
