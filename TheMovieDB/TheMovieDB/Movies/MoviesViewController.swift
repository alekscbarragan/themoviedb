//
//  MoviesViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol MoviesViewControllerDelegate: AnyObject {
    func moviesViewController(_ viewController: MoviesViewController, didSelect movie: Movie)
    func moviesViewControllerWillSearch(_ viewController: MoviesViewController)
}

class MoviesViewController: UIViewController {
    
    weak var delegate: MoviesViewControllerDelegate?
    var favoritesRepository: MoviesRepository<Movie>?
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        return refreshControl
    }()

    private(set) lazy var moviesView: MoviesView = {
        let view = MoviesView()
        return view
    }()
    
    let networkClient: NetworkClient
    var navigationTitle: String {
        return "Popular"
    }

    let repository: MoviesRepository<Movie>

    // MARK: - Life View Controller Cycle
    init(networkClient: NetworkClient, repository: MoviesRepository<Movie>) {
        self.repository = repository
        self.networkClient = networkClient
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = moviesView
    }
    
    @objc func searchTouchUpInside() {
        delegate?.moviesViewControllerWillSearch(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(navigationTitle)
        moviesView.refreshControl = refreshControl
        
        let searchBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchTouchUpInside))
        navigationItem.rightBarButtonItem = searchBarButton
        
        moviesView.collectionViewDataSource = self
        moviesView.collectionViewDelegate = self
        moviesView.register(cellType: MovieCollectionViewCell.self)
        moviesView.reloadData()
        networkRequest()
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.moviesView.reloadData()
        }
    }
    
    func setNavigationTitle(_ title: String) {
        navigationItem.title = title
    }
    
    @objc private func didPullToRefresh(_ sender: Any) {
        
    }
    
    func networkRequest() {
        networkClient.popularMovies { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let movies):
                self.repository.update(newItems: movies)
                self.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc private func didUpdateMovies() {
        moviesView.reloadData()
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return repository.numberOfItems(inSection: section)
    }
    
    func movie(at indexPath: IndexPath) -> Movie? {
        return repository.item(at: indexPath)
    }
}

// MARK: - UICollectionViewDataSource
// MARK: -
extension MoviesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItemsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(for: indexPath) as MovieCollectionViewCell
        
        guard let movie = movie(at: indexPath) else { return cell }
        
        cell.titleLabel.text = movie.title
        cell.rateLabel.text = "\(movie.voteAverage)"
        cell.delegate = self
        cell.imageView.downloadImage(path: movie.posterPath)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate
// MARK: -
extension MoviesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        guard let movie = movie(at: indexPath) else { return }
        delegate?.moviesViewController(self, didSelect: movie)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
// MARK: -
extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var padding: CGFloat = 40.0
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = (layout.sectionInset.left + layout.sectionInset.right) * 1.5
        }
        let width = (collectionView.frame.width - padding) / 2
        let size = CGSize(width: width, height: 300)
        return size
    }
}

// MARK: - UICollectionViewDelegate
// MARK: -
extension MoviesViewController: MovieCollectionViewCellDelegate {
    func movieCollectionViewCell(_ cell: MovieCollectionViewCell, didSelectFavorite button: UIButton) {
        guard let indexPath = moviesView.indexPath(for: cell) else {
            print("`IndexPath` for cell: \(cell) not found.")
            return
        }

        guard var movie = movie(at: indexPath) else { return }
        movie.isFavorited = button.isSelected
        print("Favorited movie id: \(movie.id)")
        didFavorite(movie: movie)
    }
    
    func movieCollectionViewCell(_ cell: MovieCollectionViewCell, didSelectBookmark sender: UIButton) {
        guard let indexPath = moviesView.indexPath(for: cell) else {
            print("`IndexPath` for cell: \(cell) not found.")
            return
        }

        guard var movie = movie(at: indexPath) else { return }
        movie.isBookmarked = sender.isSelected
        print("Bookmared movie id: \(movie.id)")
        didBookmark(movie: movie)
    }
}

// MARK: - Favorited & bookmared movie
// MARK: -
extension MoviesViewController {
    /// Handles favorte `Movie` storage.
    func didFavorite(movie: Movie) {
        favoritesRepository?.add(item: movie, inSection: 0)
        Notifications.didUpdateWatchList.post()
    }
    
    /// Handles bookmarked `Movie` storage.
    func didBookmark(movie: Movie) {
        favoritesRepository?.add(item: movie, inSection: 1)
        Notifications.didUpdateWatchList.post()
    }
}
