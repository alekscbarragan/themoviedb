//
//  SearchViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class SearchViewController: MoviesViewController {

    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.delegate = self
        searchController.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
        return searchController
    }()
    
    private lazy var searchDataSource = MoviesRepository<Movie>()
    private var searchMovieQuery: String?
    var isSearchActive: Bool = false
    
    private var searchWorkItem: DispatchWorkItem?
    
    lazy var searchButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.text, for: .normal)
        button.addTarget(self, action: #selector(searchTouchUpInside), for: .touchUpInside)
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle("Search")
        searchButton.setTitle("Tap to Search", for: .normal)
    }
    
    override func searchTouchUpInside() {
        searchController.isActive = true
        searchController.searchBar.becomeFirstResponder()
    }
    
    override func networkRequest() {
        print(#function)
        guard let query = searchMovieQuery else { return }
        networkClient.searchBy(name: query) { (result) in
            switch result {
            case .success(let movies):
                self.repository.update(newItems: movies)
                DispatchQueue.main.async {
                    self.reloadData()
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func search(with text: String) {
        print(text)
        searchMovieQuery = text
        networkRequest()
    }
    
}

// MARK: - UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text ?? ""
        if text.isEmpty {
            return
        }
        
        searchWorkItem?.cancel()
        
        let workItem = DispatchWorkItem(block: { [weak self] in
            guard let self = self else { return }
            self.search(with: text)
        })
        searchWorkItem = workItem
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: workItem)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationController?.popViewController(animated: true)
    }
}

extension SearchViewController: UISearchControllerDelegate {
    func didPresentSearchController(_ searchController: UISearchController) {
        let isHidden = !searchButton.isHidden
        let alpha: CGFloat = isHidden ? 0.0 : 1.0
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchButton.alpha = alpha
        }, completion: { _ in
            self.searchButton.isHidden = isHidden
        })
    }
}
