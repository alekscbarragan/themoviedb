//
//  MoviesView.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class MoviesView: UIView {
    
    // MARK: - Public Properties
    
    weak var refreshControl: UIRefreshControl? {
        didSet {
            collectionView.refreshControl = refreshControl
            collectionView.alwaysBounceVertical = true
        }
    }
    
    weak var collectionViewDelegate: UICollectionViewDelegate? {
        didSet {
            collectionView.delegate = collectionViewDelegate
        }
    }
    
    weak var collectionViewDataSource: UICollectionViewDataSource? {
        didSet {
            collectionView.dataSource = collectionViewDataSource
        }
    }
    
    // MARK: - Private properties
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .background
        collectionView.keyboardDismissMode = .interactive
        return collectionView
    }()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .background
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        addSubview(collectionView)
        collectionView.edgesToSuperview(usingSafeLayout: true)
    }
    
    // MARK: - Public Methods
    func indexPath(for cell: UICollectionViewCell) -> IndexPath? {
        return collectionView.indexPath(for: cell)
        
    }
    func register(cellType type: UICollectionViewCell.Type) {
        
        collectionView.register(cellType: type)
    }
    
    func reloadData() {
        collectionView.reloadData()
    }
}
