//
//  MovieCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol MovieCollectionViewCellDelegate: AnyObject {
    func movieCollectionViewCell(_ cell: MovieCollectionViewCell, didSelectFavorite button: UIButton)
    func movieCollectionViewCell(_ cell: MovieCollectionViewCell, didSelectBookmark sender: UIButton)
}

final class MovieCollectionViewCell: BaseCollectionViewCell {
    
    weak var delegate: MovieCollectionViewCellDelegate?
    
    let titleLabel: Label = {
        let label = Label(insets: .label, text: "Title")
        label.textAlignment = .center
        label.numberOfLines = 2
        label.minimumScaleFactor = 0.7
        label.font = .preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let rateLabel: UILabel = {
        let label: UILabel = .create(text: "7.5/10")
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    let favoriteButton: UIButton = {
        let button = UIButton()
        let selectedImage = UIImage(named: "ic-movie-heart-filled")
        let image = UIImage(named: "ic-movie-heart")
        button.setImage(selectedImage, for: .selected)
        button.setImage(image, for: .normal)
        button.tintColor = .control
        return button
    }()
    
    let moreButton: UIButton = {
        let button = UIButton()
        let selectedImage = UIImage(named: "ic-movie-bookmark-filled")
        let image = UIImage(named: "ic-movie-bookmark")
        button.setImage(selectedImage, for: .selected)
        button.setImage(image, for: .normal)
        button.tintColor = .control
        return button
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .gray
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let infoContainerView: UIView = {
        let view = UIView()
        return view
    }()
    
    let containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func setup() {
        super.setup()

        contentView.addSubview(containerView)
        containerView.edgesToSuperview(insets: .insetAll(inset: 8))
        
        containerView.addSubview(infoContainerView)
        infoContainerView.leftToSuperView()
        infoContainerView.bottomToSuperview()
        infoContainerView.rightToSuperView()
        infoContainerView.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        
        containerView.addSubview(imageView)
        imageView.topToSuperview()
        imageView.leftToSuperView()
        imageView.rightToSuperView()
        imageView.bottomToTop(of: infoContainerView)
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        
        // Heart button
        containerView.addSubview(favoriteButton)
        favoriteButton.topToSuperview(offset: 10)
        favoriteButton.rightToSuperView(offset: -10)
        favoriteButton.addTarget(self, action: #selector(favoriteButtonTouchUpInside(_:)), for: .touchUpInside)
        
        infoContainerView.addSubview(titleLabel)
        titleLabel.leftToSuperView()
        titleLabel.topToSuperview()
        titleLabel.bottomToSuperview()
        
        // Bookmark button
        moreButton.addTarget(self, action: #selector(bookmarkButtonTouchUpInside(_:)), for: .touchUpInside)
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.addArrangedSubview(moreButton)
        stackView.addArrangedSubview(rateLabel)
        
        infoContainerView.addSubview(stackView)
        stackView.topToSuperview()
        stackView.rightToSuperView()
        stackView.bottomToSuperview()
        stackView.leftToRight(of: titleLabel)
    }
    
    @objc private func favoriteButtonTouchUpInside(_ sender: UIButton) {
        UIButton.animateSelection(sender: sender)
        delegate?.movieCollectionViewCell(self, didSelectFavorite: sender)
    }
    
    @objc private func bookmarkButtonTouchUpInside(_ sender: UIButton) {
        UIButton.animateSelection(sender: sender)
        delegate?.movieCollectionViewCell(self, didSelectBookmark: sender)
    }
    
}
