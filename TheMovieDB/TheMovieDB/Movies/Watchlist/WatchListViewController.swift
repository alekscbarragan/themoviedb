//
//  WatchListViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class WatchListViewController: UIViewController {

    private lazy var tableView = UITableView(frame: .zero, style: .grouped)
    
    var dataSource: MoviesRepository<Movie>
    private let networkClient: NetworkClient
    private let operationQueue = OperationQueue()
    
    init(repository: MoviesRepository<Movie>, networkClient: NetworkClient) {
        self.networkClient = networkClient
        self.dataSource = repository
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = tableView
        tableView.backgroundColor = .background
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(cellType: UITableViewCell.self)
        
        Notifications.didUpdateWatchList.observe(observer: self, selector: #selector(watchListDidUpdate))
        tableView.reloadData()
    }

}

// MARK: - UITableViewDataSource
extension WatchListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(for: indexPath) as UITableViewCell
        guard let movie = dataSource.item(at: indexPath) else { return cell }
        cell.textLabel?.text = movie.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfItems(inSection: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.sections
    }
}

// MARK: - UITableViewDelegate
extension WatchListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Favorites"
            
        case 1:
            return "Watchlist"
            
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let movie = dataSource.item(at: indexPath) else { return }
        showDetail(with: movie)
    }
    
    private func showDetail(with movie: Movie) {
        let movieDetailViewController = MovieDetailViewController(movie: movie, networkClient: networkClient)
        let systemItem: UIBarButtonItem.SystemItem
        if #available(iOS 13.0, *) {
            systemItem = .close
        } else {
            // Fallback on earlier versions
            systemItem = .done
        }
        
        let closeBarButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(dismissDetailViewController))
        movieDetailViewController.navigationItem.leftBarButtonItem = closeBarButton
        let navigationController = UINavigationController(rootViewController: movieDetailViewController)
        present(navigationController, animated: true)
    }
    
    @objc private func dismissDetailViewController() {
        dismiss(animated: true)
    }
}

extension WatchListViewController {
    @objc func watchListDidUpdate() {
        print(#function)
        tableView.reloadData()
        guard let movies = dataSource.items.first, !movies.isEmpty else {
            return
        }
        
//        operationQueue.addOperation {
//            Storage.store(movies, to: .documents, as: "watchList")
//        }
    }
}
