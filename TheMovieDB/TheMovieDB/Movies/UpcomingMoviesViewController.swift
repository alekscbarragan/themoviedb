//
//  UpcomingMoviesViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class UpcomingMoviesViewController: MoviesViewController {

    override var navigationTitle: String {
        return "Upcoming Movies"
    }
    
    override func networkRequest() {
        networkClient.upcomingMovies { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let movies):
                self.repository.update(newItems: movies)
                self.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }

}
