//
//  GalleryViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 09/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol GalleryViewControllerDelegate: AnyObject {
    func galleryViewController(_ viewController: GalleryViewController, didSelect movieVideo: MovieVideo)
}

final class GalleryViewController: UIViewController {
    
    var selectedIndex: Int?
    weak var delegate: GalleryViewControllerDelegate?
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(cellType: VideoCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .systemBackground
        return collectionView
    }()
    
    private var dataSource = MoviesRepository<MovieVideo>()
    private let movieVideos: [MovieVideo]
    private var isPagingEnabled = false
    
    init(movieVideos: [MovieVideo], isPagingEnabled: Bool = false) {
        self.movieVideos = movieVideos
        super.init(nibName: nil, bundle: nil)
        self.isPagingEnabled = isPagingEnabled
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = collectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.update(newItems: movieVideos)

        if isPagingEnabled, let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.sectionInset = .zero
        }
        
        collectionView.isPagingEnabled = isPagingEnabled
        collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selectedIndex = selectedIndex {
            let indexPath = IndexPath(row: selectedIndex, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}

// MARK: - UICollectionViewDataSource
// MARK: -
extension GalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numberOfItems(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(for: indexPath) as VideoCollectionViewCell
        guard let movieVideo = dataSource.item(at: indexPath) else { return cell }
        if isPagingEnabled {
            cell.customContentMode = .scaleAspectFit
        }
        
        cell.key = movieVideo.key
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate
// MARK: -
extension GalleryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let movieVideo = dataSource.item(at: indexPath) else { return }
        delegate?.galleryViewController(self, didSelect: movieVideo)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
// MARK: -
extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isPagingEnabled {
            return CGSize(width: view.frame.width, height: view.frame.height)
        }
        
        var padding: CGFloat = 40
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = (layout.sectionInset.left + layout.sectionInset.right) * 1.5
        }
        let width = (collectionView.frame.width - padding) / 3
        return CGSize(width: width, height: collectionView.frame.height)
    }
}
