//
//  MovieDetailView.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol MovieDetailViewDelegate: AnyObject {
    func movieDetailView(_ view: MovieDetailView, didSelectMovies sender: UIButton)
}

final class MovieDetailView: UIView {
    
    weak var delegate: MovieDetailViewDelegate?

    private lazy var posterImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "placeholder")
        return imageView
    }()
    
    private let nameLabel: Label = {
        let label = Label(insets: .label, text: "Movie name")
        label.font = .preferredFont(forTextStyle: .largeTitle)
        return label
    }()
    
    private let ratingLabel: Label = {
        let label = Label(insets: .label, text: "7.5")
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let playButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "button-play-filled"), for: .normal)
        button.setTitle("Trailer", for: .normal)
        button.setTitleColor(.label, for: .normal)
        button.tintColor = .control
        return button
    }()
    
    private let synopsisLabel: Label = {
        let label = Label(insets: .label, text: "Synopsis")
        label.font = .preferredFont(forTextStyle: .body)
        label.numberOfLines = 0
        return label
    }()
    
    private let scrollView: ArrangedScrollView = {
        let scrollView = ArrangedScrollView()
        return scrollView
    }()
    
    private let videosContainerView: UIView = {
        let view = UIView()
        return view
    }()
    
    // MARK: - Life Cycle
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .background
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupConstraints()
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        addSubview(scrollView)
        scrollView.edgesToSuperview(usingSafeLayout: true)

        let stackView = UIStackView(arrangedSubviews: [ratingLabel, playButton])
        stackView.axis = .horizontal
        playButton.isHidden = true
        
        let subviews = [posterImageView, nameLabel, stackView, videosContainerView, synopsisLabel]
        scrollView.addArranged(subviews: subviews)
        posterImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.6).isActive = true
        videosContainerView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        videosContainerView.isHidden = true
        playButton.addTarget(self, action: #selector(playTouchUpInside(_:)), for: .touchUpInside)
    }
    
    private func shouldShowPlayButton(shouldShow: Bool) {
        UIViewPropertyAnimator(duration: 0.33, dampingRatio: 0.65) {
            self.playButton.isHidden = !shouldShow
        }.startAnimation()
    }
    
    private func shouldShowVideoGallery(shouldShow: Bool) {
        let alpha: CGFloat = shouldShow ? 0.0 : 1.0
        UIViewPropertyAnimator(duration: 0.33, dampingRatio: 0.65) {
            self.videosContainerView.isHidden = shouldShow
            self.videosContainerView.alpha = alpha
        }.startAnimation()
    }
    
    @objc private func playTouchUpInside(_ sender: UIButton) {
        shouldShowVideoGallery(shouldShow: !videosContainerView.isHidden)
        delegate?.movieDetailView(self, didSelectMovies: sender)
    }
    
    // MARK: - Public Methods
    func addVideoGalleryView(_ view: UIView) {
        videosContainerView.addSubview(view)
        view.edgesToSuperview()
    }
    
    func showPlay() {
        shouldShowPlayButton(shouldShow: true)
    }
    
    func hidePlay() {
        shouldShowPlayButton(shouldShow: false)
    }
    
    func didUpdateLayout() {
        scrollView.didUpdateLayout()
    }
    
    func configure(posterPath: String?, name: String, rating: String, synopsis: String, placeholderImage: UIImage?) {
        nameLabel.text = name
        ratingLabel.text = "Rating: " + rating
        synopsisLabel.text = synopsis
        posterImageView.isHidden = true
        
        guard let posterPath = posterPath else { return }
        
        posterImageView.downloadImage(path: posterPath) { (success) in
            guard success else { return }
            DispatchQueue.main.async {
                UIViewPropertyAnimator(duration: 1.5, dampingRatio: 0.5) {
                    self.posterImageView.isHidden = false
                }.startAnimation()
            }
        }
    }
    
}
