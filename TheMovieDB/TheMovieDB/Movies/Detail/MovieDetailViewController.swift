//
//  MovieDetailViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import AVKit
import UIKit
import SafariServices

protocol MovieDetailViewControllerDelegate: AnyObject {
    func movieDetailViewController(_ viewController: MovieDetailViewController, willPlay url: URL)
}

final class MovieDetailViewController: UIViewController {

    weak var delegate: MovieDetailViewControllerDelegate?
    
    private lazy var detailView = MovieDetailView()
    private let networkClient: NetworkClient
    private let movie: Movie
    private let image: UIImage?
    
    private var galleryViewController: GalleryViewController?
    
    init(movie: Movie, networkClient: NetworkClient, image: UIImage? = nil) {
        self.networkClient = networkClient
        self.movie = movie
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = detailView
        detailView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailView.configure(posterPath: movie.posterPath,
                             name: movie.title,
                             rating: String(movie.voteAverage),
                             synopsis: movie.overview,
                             placeholderImage: image)
        
        networkClient.movieVideos(withId: String(movie.id)) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let movieVideos):
                
                print("movie videos count: \(movieVideos.count)")
                print(movieVideos)
                
                if movieVideos.isEmpty {
                    return
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.detailView.showPlay()
                    let galleryViewController = GalleryViewController(movieVideos: movieVideos)
                    self.addChild(galleryViewController)
                    self.detailView.addVideoGalleryView(galleryViewController.view)
                    galleryViewController.didMove(toParent: self)
                    galleryViewController.delegate = self
                    self.galleryViewController = galleryViewController
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        detailView.didUpdateLayout()
    }

}

extension MovieDetailViewController: MovieDetailViewDelegate {
    func movieDetailView(_ view: MovieDetailView, didSelectMovies sender: UIButton) {
        print(#function)
    }
}

extension MovieDetailViewController: GalleryViewControllerDelegate {
    func galleryViewController(_ viewController: GalleryViewController, didSelect movieVideo: MovieVideo) {
        print(#function)
        guard let url = movieVideo.youtubeUrl else {
            print("❌ Invalid youtube URL.")
            return
        }
        
        if let delegate = delegate {
            delegate.movieDetailViewController(self, willPlay: url)
        } else {
            let safariViewController = SFSafariViewController(url: url)
            safariViewController.preferredControlTintColor = .control
            present(safariViewController, animated: true)
        }
        
    }
}
