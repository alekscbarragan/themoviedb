//
//  VideoCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 09/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class VideoCollectionViewCell: UICollectionViewCell {
    
    var key: String = "" {
        didSet {
            if key.isEmpty {
                return
            }
            
            let urlString = "https://img.youtube.com/vi/\(key)/0.jpg"
            imageView.downloadImage(withUrlString: urlString)
        }
    }
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5.0
        return imageView
    }()
    
    private let playButton: UIButton = {
        let button = UIButton()
        let image = UIImage(named: "button-play-filled")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = .control
        button.isUserInteractionEnabled = false
        return button
    }()
    
    var customContentMode: UIView.ContentMode = .scaleAspectFill {
        didSet {
            imageView.contentMode = customContentMode
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        contentView.addSubview(imageView)
        imageView.edgesToSuperview()
        
        contentView.addSubview(playButton)
        playButton.edgesToSuperview()
    }
}
