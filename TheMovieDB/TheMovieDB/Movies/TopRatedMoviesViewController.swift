//
//  TopRatedMoviesViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

class TopRatedMoviesViewController: MoviesViewController {
    override var navigationTitle: String {
        return "Top Rated"
    }
    
    override func networkRequest() {
        networkClient.topRatedMovies { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let movies):
                self.repository.update(newItems: movies)
                self.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
