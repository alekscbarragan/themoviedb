//
//  MovieVideo.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 09/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct MovieVideo: Codable {
    let id: String
    let key: String
    let name: String
    let type: String
    let site: String
}

extension MovieVideo: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
}

extension MovieVideo {
    var youtubeUrl: URL? {
        return URL(string: "https://www.youtube.com/watch?v=\(key)")
    }
}

struct MovieVideosResponse: Codable {
    let id: Int
    let results: [MovieVideo]
}
