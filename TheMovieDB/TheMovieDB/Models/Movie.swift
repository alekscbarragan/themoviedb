//
//  Movie.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct Movie: Codable {
    let popularity: Double
    let voteCount: Int
    let posterPath: String?
    let originalTitle: String
    let title: String
    let id: Int
    let backdropPath: String?
    let genreIds: [Int]
    let voteAverage: Double
    let overview: String
    let releaseDate: String
    
    var isFavorited: Bool? = false
    var isBookmarked: Bool? = false
    
    enum CodingKeys: String, CodingKey {
        case popularity
        case voteCount = "vote_count"
        case posterPath = "poster_path"
        case originalTitle = "original_title"
        case title
        case id
        case backdropPath = "backdrop_path"
        case genreIds = "genre_ids"
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
        // custom for encoding
        case isFavorited
        case isBookmarked
    }
}

extension Movie: Equatable {
    static func == (lhs: Movie, rhs: Movie) -> Bool {
        return lhs.id == rhs.id
    }
}
