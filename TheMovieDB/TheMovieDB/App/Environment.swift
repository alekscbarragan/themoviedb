//
//  Environment.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum Environment {
    case debug
    case release
    
    static var current: Environment {
        #if DEBUG
        return .debug
        #else
        return .release
        #endif
    }
    
    static var baseURLString: String {
        switch current {
        case .debug:
            return "https://api.themoviedb.org/3"
            
        case .release:
            // TODO: Replace production URL.
            return "https://api.themoviedb.org/3"
        }
    }
    
    static var imageBaseUrlString: String {
        switch current {
        case .debug:
            return "https://image.tmdb.org/t/p/original"
            
        case .release:
            // TODO: Replace production URL.
            return "https://image.tmdb.org/t/p/original"
        }
    }
    
    static var apiKey: String {
        return "adeefc70c416e66c04fdc31abf034d9d"
    }
}
