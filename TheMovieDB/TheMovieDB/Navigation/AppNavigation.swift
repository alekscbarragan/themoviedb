//
//  AppNavigation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class AppNavigation {
    
    weak var window: UIWindow?
    
    let networkClient: NetworkClient = TheMovieDBNetworkClient()
    
    private lazy var popularMoviesCoordinator: Coordinator = {
        let repository = MoviesRepository<Movie>()
        let navigationController = UINavigationController()
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.tabBarItem.title = "Popular"
        navigationController.tabBarItem.selectedImage = UIImage(named: "ic-movie-tab-filled")
        navigationController.tabBarItem.image = UIImage(named: "ic-movie-tab")
        
        let coordinator = PopularMoviesCoordinator(navigationController: navigationController,
                                                   repository: repository,
                                                   networkClient: networkClient)
        coordinator.favoritesRepository = favoritesRepository
        return coordinator
    }()
    
    private lazy var topRatedMoviesCoordinator: Coordinator = {
        let repository = MoviesRepository<Movie>()
        let navigationController = UINavigationController()
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.tabBarItem.title = "Top Rated"
        navigationController.tabBarItem.selectedImage = UIImage(named: "ic-movie-topRated-filled")
        navigationController.tabBarItem.image = UIImage(named: "ic-movie-topRated")
        
        let coordinator = TopRatedMoviesCoordinator(navigationController: navigationController,
                                                   repository: repository,
                                                   networkClient: networkClient)
        coordinator.favoritesRepository = favoritesRepository
        return coordinator
    }()
    
    private lazy var upcomingMoviesCoordinator: Coordinator = {
        let repository = MoviesRepository<Movie>()
        let navigationController = UINavigationController()
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.tabBarItem.title = "Upcoming"
        navigationController.tabBarItem.selectedImage = UIImage(named: "ic-movie-upcoming-filled")
        navigationController.tabBarItem.image = UIImage(named: "ic-movie-upcoming")
        
        let coordinator = UpcomingMoviesCoordinator(navigationController: navigationController,
                                                   repository: repository,
                                                   networkClient: networkClient)
        coordinator.favoritesRepository = favoritesRepository
        return coordinator
    }()
    
    lazy var favoritesRepository = MoviesRepository<Movie>()
    
    init(window: UIWindow?) {
        self.window = window
        
        // Appearance
        UITabBar.appearance().tintColor = .control
        UINavigationBar.appearance().tintColor = .control
        
        // Start coordinators
        let coordinators = [popularMoviesCoordinator, topRatedMoviesCoordinator, upcomingMoviesCoordinator]
        coordinators.forEach { $0.start() }
        
        // Container
        let tabController = UITabBarController()
        tabController.viewControllers = coordinators.map { $0.navigationController }
        
        let centerViewController = CenterViewController(rootViewController: tabController)
        tabController.viewControllers?
            .compactMap { ($0 as? UINavigationController)?.topViewController }
            .forEach { centerViewController.configure($0) }
        
        // Side menu
        let menuViewController = WatchListViewController(repository: favoritesRepository, networkClient: networkClient)
        
        // Root view container
        let containerViewController = ContainerViewController(centerViewController: centerViewController,
                                                              menuViewController: menuViewController)
        
        window?.rootViewController = containerViewController
    }
}
