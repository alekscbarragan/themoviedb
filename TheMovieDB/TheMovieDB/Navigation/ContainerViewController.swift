//
//  ContainerViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class ContainerViewController: UIViewController {
    let centerViewController: CenterViewController
    let menuViewController: UIViewController
    var isMenuExpanded = false
    
    private let menuBackgroundView = UIView()
    
    init(centerViewController: CenterViewController, menuViewController: UIViewController) {
        self.centerViewController = centerViewController
        self.menuViewController = menuViewController
        super.init(nibName: nil, bundle: nil)
        centerViewController.menuDelegate = self
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(centerViewController)
        view.addSubview(centerViewController.view)
        centerViewController.didMove(toParent: self)
        
        menuBackgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(menuBackgroundView)
        
        addChild(menuViewController)
        view.addSubview(menuViewController.view)
        menuViewController.didMove(toParent: self)
        
        configureSwipeGesture()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let width: CGFloat = isMenuExpanded ? (view.bounds.width * (2 / 3)) : 0.0
        menuViewController.view.frame = CGRect(x: 0, y: 0, width: width, height: view.bounds.height)
        
        let menuBackgroundViewWidth = isMenuExpanded ? view.bounds.width : 0.0
        menuBackgroundView.frame = CGRect(x: 0, y: 0, width: menuBackgroundViewWidth, height: view.bounds.height)
        menuBackgroundView.alpha = isMenuExpanded ? 0.7 : 1.0
        
        let centerXOrigin: CGFloat = isMenuExpanded ? width : 0.0
        var centerFrame = centerViewController.view.frame
        centerFrame.origin.x = centerXOrigin
        centerViewController.view.frame = centerFrame
    }
    
    private func configureSwipeGesture() {
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeLeft))
        swipeLeftGesture.direction = .left
        menuBackgroundView.addGestureRecognizer(swipeLeftGesture)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOverlay))
        menuBackgroundView.addGestureRecognizer(tapGesture)
        
    }
    @objc private func didSwipeLeft() {
        toggleMenu()
    }

    @objc private func didTapOverlay() {
        toggleMenu()
    }
    
    func toggleMenu() {
        isMenuExpanded = !isMenuExpanded
        let bounds = view.bounds
        let width: CGFloat = (isMenuExpanded) ? bounds.width * 2 / 3 : 0.0
        
        let menuBackgroundViewWidth = isMenuExpanded ? view.bounds.width : 0.0
        let menuBackgroundViewAlpha: CGFloat = isMenuExpanded ? 1.0 : 0.0
        menuBackgroundView.frame = CGRect(x: 0, y: 0, width: menuBackgroundViewWidth, height: bounds.height)
        
        let centerXOrigin: CGFloat = isMenuExpanded ? width : 0.0
        var centerFrame = centerViewController.view.frame
        centerFrame.origin.x = centerXOrigin
        
        UIView.animate(withDuration: 0.3, animations: {
            self.menuViewController.view.frame = CGRect(x: 0, y: 0, width: width, height: bounds.height)
            self.menuBackgroundView.alpha = menuBackgroundViewAlpha
            self.centerViewController.view.frame = centerFrame
            
        }, completion: { (_) in
            
        })
    }
}

extension ContainerViewController: CenterViewControllerDelegate {
    func centerViewControllerDidTapMenuButton(_ viewController: CenterViewController) {
        toggleMenu()
    }
}
