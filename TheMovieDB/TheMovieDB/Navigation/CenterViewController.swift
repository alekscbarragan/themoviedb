//
//  CenterViewController.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

protocol CenterViewControllerDelegate: AnyObject {
    func centerViewControllerDidTapMenuButton(_ viewController: CenterViewController)
}

final class CenterViewController: UINavigationController, UINavigationControllerDelegate {
    
    var button: UIBarButtonItem?
    weak var menuDelegate: CenterViewControllerDelegate?
    
    /// Create an instace of CenterViewController. If `rootViewController` is a `UITabBarController` it will set `navigationBar.isHidden` to `true`.
    /// The reason of this, it is because the `UITabBarController` might have an instance of `UINavigationController` for one of its `viewControllers`.
    /// You must set `navigationBar.isHidden` to `false` after initialization if this not the behaviour you desire.
    /// - Parameter rootViewController: The `rootViewController` of the `UINavigationController` subclass.
    /// - Parameter menuBarButton: `menuBarButton` you want to pass, otherwise it will set a default set withint the class. You must use the `menuDelegate` to catch to this action.
    init(rootViewController: UIViewController, menuBarButton: UIBarButtonItem? = nil) {
//        super.init(rootViewController: rootViewController)
        super.init(nibName: nil, bundle: nil)
        pushViewController(rootViewController, animated: false)
        if rootViewController is UITabBarController {
            navigationBar.isHidden = true
        }
        
        if let menuBarButton = menuBarButton {
            button = menuBarButton
        } else {
            button = UIBarButtonItem(image: UIImage(named: "menu-burger"), style: .plain, target: self, action: #selector(menuButtonTapped))
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        button = UIBarButtonItem(image: UIImage(named: "menu-burger"), style: .plain, target: self, action: #selector(menuButtonTapped))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        prepareForNavigation()
    }
    
    func configure(_ viewController: UIViewController?) {
        guard let _viewController = viewController else {
            print("Could not configure viewcontroller: \(type(of: viewController))")
            return
        }
        
        var barButtons = [button]
        if let viewControllerLeftBarButtonItems = _viewController.navigationItem.leftBarButtonItems {
            barButtons += viewControllerLeftBarButtonItems
        }
        
        _viewController.navigationItem.leftBarButtonItems = barButtons.compactMap { $0 }
    }
    
    @objc private func menuButtonTapped() {
        menuDelegate?.centerViewControllerDidTapMenuButton(self)
    }
    
    private func prepareForNavigation() {
        topViewController?.navigationItem.title = topViewController?.title
        if viewControllers.count < 2 {
            topViewController?.navigationItem.leftBarButtonItem = button
        }
    }
}
