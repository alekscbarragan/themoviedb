//
//  PopularMoviesCoordinator.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import SafariServices
import UIKit

final class PopularMoviesCoordinator: Coordinator {
    let navigationController: UINavigationController
    private let repository: MoviesRepository<Movie>
    private let networkClient: NetworkClient
    private lazy var searchRepository = MoviesRepository<Movie>()
    var favoritesRepository: MoviesRepository<Movie>?
    
    init(navigationController: UINavigationController, repository: MoviesRepository<Movie>, networkClient: NetworkClient) {
        self.navigationController = navigationController
        self.repository = repository
        self.networkClient = networkClient
    }
    
    func start() {
        let moviesViewController = MoviesViewController(networkClient: networkClient, repository: repository)
        moviesViewController.delegate = self
        moviesViewController.favoritesRepository = favoritesRepository
        navigationController.pushViewController(moviesViewController, animated: false)
    }
}

// MARK: - MoviesViewControllerDelegate
// MARK: -
extension PopularMoviesCoordinator: MoviesViewControllerDelegate {
    func moviesViewControllerWillSearch(_ viewController: MoviesViewController) {
        let searchViewController = SearchViewController(networkClient: networkClient, repository: searchRepository)
        searchViewController.delegate = self
        navigationController.pushViewController(searchViewController, animated: true)
    }
    
    func moviesViewController(_ viewController: MoviesViewController, didSelect movie: Movie) {
        let movieDetailViewController = MovieDetailViewController(movie: movie, networkClient: networkClient)
        movieDetailViewController.delegate = self
        navigationController.pushViewController(movieDetailViewController, animated: true)
    }
}

// MARK: - MoviesViewControllerDelegate
// MARK: -
extension PopularMoviesCoordinator: MovieDetailViewControllerDelegate {
    func movieDetailViewController(_ viewController: MovieDetailViewController, willPlay url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.preferredControlTintColor = .control
        navigationController.present(safariViewController, animated: true)
    }
}
