//
//  HttpMethod.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    
    var stringValue: String {
        return rawValue
    }
}
