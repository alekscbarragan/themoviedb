//
//  SearchMovieOperation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class SearchMovieOperation: BaseNetworkOperation<MovieResponse> {
    override var endpoint: EndpointConvertible {
        return SearchMovieEndpoint(parameters: parameters)
    }
    
    override var parameters: [String: Any]? {
        return ["query": query, "language": "en-US"]
    }
    
    let query: String
    
    init(query: String, completion: @escaping Completion) {
        self.query = query
        super.init(completion: completion)
    }
}
