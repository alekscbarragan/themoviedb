//
//  MovieVideosOperation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 09/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class MovieVideosOperation: BaseNetworkOperation<MovieVideosResponse> {
    override var endpoint: EndpointConvertible {
        return MovieVideosEndpoint(movieId: movieId, parameters: parameters)
    }
    override var parameters: [String: Any]? {
        return ["language": "en-US"]
    }
    
    let movieId: String
    
    init(movieId: String, completion: @escaping Completion) {
        self.movieId = movieId
        super.init(completion: completion)
    }
}
