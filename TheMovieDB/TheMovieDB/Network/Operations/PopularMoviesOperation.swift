//
//  PopularMoviesOperation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class PopularMoviesOperation: BaseNetworkOperation<MovieResponse> {
    override var endpoint: EndpointConvertible {
        return PopularMoviesEndpoint(parameters: parameters)
    }
    override var parameters: [String: Any]? {
        return ["language": "en-US"]
    }
}
