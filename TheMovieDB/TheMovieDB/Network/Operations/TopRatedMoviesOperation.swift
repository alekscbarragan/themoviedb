//
//  TopRatedMoviesOperation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class TopRatedMoviesOperation: BaseNetworkOperation<MovieResponse> {
    override var endpoint: EndpointConvertible {
        return TopRatedMoviesEndpoint(parameters: parameters)
    }
    override var parameters: [String: Any]? {
        return ["language": "en-US", "page": "1"]
    }
}
