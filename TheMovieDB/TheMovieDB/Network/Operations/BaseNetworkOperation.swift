//
//  BaseNetworkOperation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

class BaseNetworkOperation<T: Codable>: BaseOperation {
    
    // MARK: - Private Properties
    // MARK: -
    private var task: URLSessionDataTask?
    
    // MARK: - Public Properties
    // MARK: -
    
    var session: URLSession
    
    var endpoint: EndpointConvertible {
        return NullEndpoint()
    }
    
    var parameters: [String: Any]? {
        return nil
    }
    
    var generatedModel: T?
    
    typealias Completion = (Result<T, Error>) -> Void
    var completion: Completion?
    
    // MARK: - Init
    // MARK: -
    init(session: URLSession = URLSession.shared, completion: Completion? = nil) {
        self.session = session
        self.completion = completion
        super.init()
    }
    
    // MARK: - Overrides
    // MARK: -
    override func cancel() {
        switch state {
        case .executing:
            print("There's a current task on going, cancelling it.")
            task?.cancel()
            
        default:
            print("No data task to cancel")
        }
        
        super.cancel()
    }
    
    open override func execute() {
        
        if isCancelled {
            finish(with: [OperationError.cancelled])
            return
        }
        
        guard let url = getUrl() else {
            finish(with: [OperationError.invalidUrl])
            return
        }
        
        guard let request = getRequest(with: url) else {
            finish(with: [OperationError.badParameters])
            return
        }
        
        print("Will start data task with URL: \(url.absoluteString)")
        
        let task = session.dataTask(with: request) { [weak self] (data, _, error) in
            guard let self = self else { return }
            
            if self.isCancelled {
                let error = OperationError.cancelled
                self.finish(with: [error])
                return
            }
            
            if let error = error {
                self.finish(with: [error])
                return
            }
            
            guard let data = data else {
                let error = OperationError.dataIsNil
                self.finish(with: [error])
                return
            }
            
            self.finishedDownloadingData(data: data)
            
        }
        
        task.resume()
        self.task = task
    }
    
    // MARK: - Private Methods
    // MARK: -
    private func getUrl() -> URL? {
        if let parameters = parameters, endpoint.httpMethod == .get {
            return try? endpoint.toURL(params: parameters)
        }
        
        return try? endpoint.toURL()
    }
    
    private func getRequest(with url: URL) -> URLRequest? {
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.httpMethod.stringValue
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        guard let parameters = parameters else {
            return request
        }
        
        guard endpoint.httpMethod == .post  else {
            return request
        }
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else {
            return nil
        }
        
        request.httpBody = httpBody
        
        return request
    }
    
    /// Decodes `Data` into an object. Override if custom decoding strategy will be used.
    /// By default, JSONDecodeer it is used.
    /// `finish()` or `finish(with:)` must be called to finish operation.
    func finishedDownloadingData(data: Data) {
        let decoder = JSONDecoder()
        
//        if let rawString = String(data: data, encoding: .utf8) {
//            print("👀 Raw Data STring", rawString, "\n\n")
//        }
        
        do {
            let model = try decoder.decode(T.self, from: data)
            generatedModel = model
            didGenerate(model: model)
            completion?(.success(model))
            finish()
        } catch {
            completion?(.failure(error))
            finish(with: [error])
        }
    }
    
    // MARK: - Public Methods
    // MARK: -
    
    /// Override method to handle custom logic for generated model.
    /// You don't need call super.
    func didGenerate(model: T) { }
}
