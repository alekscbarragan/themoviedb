//
//  OperationError.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum OperationError: LocalizedError {
    case cancelled
    case invalidUrl
    case badParameters
    case dataIsNil
    case incorrectDataFormat
}
