//
//  PopularMoviesEndpoint.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct PopularMoviesEndpoint: EndpointConvertible {
    var httpMethod: HttpMethod {
        return .get
    }
    
    var path: String {
        return "/movie/popular"
    }
    
    var parameters: [String: Any]?
    
    init(parameters: [String: Any]?) {
        self.parameters = parameters
    }
}
