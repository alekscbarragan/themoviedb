//
//  TopRatedMoviesEndpoint.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct TopRatedMoviesEndpoint: EndpointConvertible {

    var path: String {
        return "/movie/top_rated"
    }
    
    var parameters: [String: Any]?
    
    init(parameters: [String: Any]?) {
        self.parameters = parameters
    }
}
