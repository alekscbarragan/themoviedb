//
//  MovieVideosEndpoint.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 09/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct MovieVideosEndpoint: EndpointConvertible {
    var httpMethod: HttpMethod {
        return .get
    }
    
    var path: String {
        return "/movie/\(movieId)/videos"
    }
    
    var parameters: [String: Any]?
    let movieId: String
    
    init(movieId: String, parameters: [String: Any]?) {
        self.movieId = movieId
        self.parameters = parameters
    }
}
