//
//  SearchMovieEndpoint.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct SearchMovieEndpoint: EndpointConvertible {
    var path: String {
        return "/search/movie"
    }
    
    var parameters: [String: Any]?
    
    init(parameters: [String: Any]?) {
        self.parameters = parameters
    }
}
