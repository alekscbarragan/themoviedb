//
//  NullEndpoint.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

struct NullEndpoint: EndpointConvertible {
    var path: String {
        return ""
    }
    
    var httpMethod: HttpMethod {
        return .get
    }
    
    var baseURLString: String {
        return "https://httpbin.org"
    }
    
    func toURL() throws -> URL {
        throw EndpointError.invalidURL
    }
    
    func toURL(params: [String: Any]) throws -> URL {
        throw EndpointError.invalidURL
    }
}
