//
//  EndpointConvertible.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol EndpointConvertible {
    var baseURLString: String { get }
    var path: String { get }
    var httpMethod: HttpMethod { get }
    var apiKey: String { get }
    
    func toURL() throws -> URL
    func toURL(params: [String: Any]) throws -> URL
}

extension EndpointConvertible {
    func toURL() throws -> URL {
        guard var url = URL(string: baseURLString) else {
            throw EndpointError.invalidURL
        }
        url.appendPathComponent(path)
        return url
    }
    
    func toURL(params: [String: Any]) throws -> URL {
        guard var parameters = params as? [String: String] else {
            throw EndpointError.invalidParameters
        }
        
        parameters["api_key"] = apiKey
        
        let encodedParams = parameters.toURLEncodedString()
        guard let url = URL(string: "\(baseURLString)\(path)?\(encodedParams)") else {
            throw EndpointError.invalidURL
        }
        
        return url
    }
}

extension EndpointConvertible {
    var baseURLString: String {
        return Environment.baseURLString
    }
    
    var apiKey: String {
        return Environment.apiKey
    }
    
    var httpMethod: HttpMethod {
        return .get
    }
}
