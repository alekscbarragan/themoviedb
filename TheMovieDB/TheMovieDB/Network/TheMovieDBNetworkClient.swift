//
//  TheMovieDBNetworkClient.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

final class TheMovieDBNetworkClient: NetworkClient {
    
    private lazy var cache = NSCache<NSString, NSData>()
    private let operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    func movieVideos(withId id: String, completion: @escaping (Result<[MovieVideo], Error>) -> Void) {
        let operation = MovieVideosOperation(movieId: id) { (result) in
            switch result {
            case .success(let movieResponse):
                completion(.success(movieResponse.results))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    func popularMovies(completion: @escaping (Result<[Movie], Error>) -> Void) {
        
        let key = "\(#function)"
        
        if let searchResponse = restoreData(forKey: key, type: MovieResponse.self) {
            print("👀 returning data from cache for key: \(key)")
            let movies = searchResponse.results
            completion(.success(movies))
            return
            
        } else if Storage.fileExists(key, in: .caches) {
            print("👀 returning data from DISK for key: \(key)")
            if let searchResponse = Storage.retrieve(key, from: .caches, as: MovieResponse.self) {
                print("👀 did read data from DISK for key: \(key)")
                operationQueue.addOperation {
                    self.storeData(searchResponse, key: key)
                }
                
                let movies = searchResponse.results
                completion(.success(movies))
                return
            }
        }
        
        let operation = PopularMoviesOperation { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let moviesResponse):
                self.cacheData(object: moviesResponse, forKey: key)
                completion(.success(moviesResponse.results))
                
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    func topRatedMovies(completion: @escaping (Result<[Movie], Error>) -> Void) {
        
        let key = "\(#function)"
        
        if let searchResponse = restoreData(forKey: key, type: MovieResponse.self) {
            print("👀 returning data from cache for key: \(key)")
            let movies = searchResponse.results
            completion(.success(movies))
            return
            
        } else if Storage.fileExists(key, in: .caches) {
            print("👀 returning data from DISK for key: \(key)")
            if let searchResponse = Storage.retrieve(key, from: .caches, as: MovieResponse.self) {
                print("👀 did read data from DISK for key: \(key)")
                operationQueue.addOperation {
                    self.storeData(searchResponse, key: key)
                }
                
                let movies = searchResponse.results
                completion(.success(movies))
                return
            }
        }
        
        let operation = TopRatedMoviesOperation { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let moviesResponse):
                self.cacheData(object: moviesResponse, forKey: key)
                completion(.success(moviesResponse.results))
                
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    func upcomingMovies(completion: @escaping (Result<[Movie], Error>) -> Void) {
        let key = "\(#function)"
        
        if let searchResponse = restoreData(forKey: key, type: MovieResponse.self) {
            print("👀 returning data from cache for key: \(key)")
            let movies = searchResponse.results
            completion(.success(movies))
            return
            
        } else if Storage.fileExists(key, in: .caches) {
            print("👀 returning data from DISK for key: \(key)")
            if let searchResponse = Storage.retrieve(key, from: .caches, as: MovieResponse.self) {
                print("👀 did read data from DISK for key: \(key)")
                operationQueue.addOperation {
                    self.storeData(searchResponse, key: key)
                }
                
                let movies = searchResponse.results
                completion(.success(movies))
                return
            }
        }
        
        let operation = UpcomingMoviesOperation { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let moviesResponse):
                self.cacheData(object: moviesResponse, forKey: key)
                completion(.success(moviesResponse.results))
                
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    func searchBy(name: String, completion: @escaping (Result<[Movie], Error>) -> Void) {
        
        if let searchResponse = restoreData(forKey: name, type: MovieResponse.self) {
            print("👀 returning data from cache for key: \(name)")
            let movies = searchResponse.results
            completion(.success(movies))
            return
        } else if Storage.fileExists(name, in: .caches) {
            print("👀 returning data from DISK for key: \(name)")
            if let searchResponse = Storage.retrieve(name, from: .caches, as: MovieResponse.self) {
                print("👀 did read data from DISK for key: \(name)")
                operationQueue.addOperation {
                    self.storeData(searchResponse, key: name)
                }
                
                let movies = searchResponse.results
                completion(.success(movies))
                return
            }
        }
        
        let operation = SearchMovieOperation(query: name) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let moviesResponse):
                self.cacheData(object: moviesResponse, forKey: name)
                completion(.success(moviesResponse.results))
                
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
        
        operationQueue.addOperation(operation)
    }

}

// MARK: - Cache
// MARK: -
extension TheMovieDBNetworkClient {
    private func storeData<Object: Codable>(_ data: Object, key: String) {
        if let data = try? JSONEncoder().encode(data) {
            print("🦄 storing data in cache for key: \(key)")
            self.cache.setObject(data as NSData, forKey: key as NSString)
        }
    }
    
    private func restoreData<Object: Codable>(forKey key: String, type: Object.Type) -> Object? {
        
        if let data = cache.object(forKey: key as NSString) {
            let object = try? JSONDecoder().decode(type, from: data as Data)
            return object
        }
        
        return nil
    }
    
    private func cacheData<Object: Codable>(object: Object, forKey key: String) {
        let storeInCacheOperation = BlockOperation {
            self.storeData(object, key: key)
        }

        let storeInDiskOperation = BlockOperation {
            Storage.store(object, to: .caches, as: key)
        }
        
        operationQueue.addOperations([storeInCacheOperation, storeInDiskOperation], waitUntilFinished: false)
    }
}
