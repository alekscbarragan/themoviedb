//
//  NetworkClient.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol NetworkClient {
    func movieVideos(withId id: String, completion: @escaping (Result<[MovieVideo], Error>) -> Void)
    func popularMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func topRatedMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func upcomingMovies(completion: @escaping (Result<[Movie], Error>) -> Void)
    func searchBy(name: String, completion: @escaping (Result<[Movie], Error>) -> Void)
}
