//
//  Repository.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol Repository: AnyObject {
    associatedtype Model: Equatable
    
    var items: [[Model]] { get }
    var sections: Int { get }
    
    func add(item: Model, inSection section: Int)
    func item(at indexPath: IndexPath) -> Model?
    func numberOfItems(inSection section: Int) -> Int
    func update(item: Model)
    func update(newItems: [Model], inSection section: Int)
    func remove(item: Model)
    func indexOf(item: Model) -> Int?
}
