//
//  Notifications.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

enum Notifications: String, Note {
    case didUpdateWatchList
    case didUpdateFavoriteMovies
    case didUpdateMovies
}
