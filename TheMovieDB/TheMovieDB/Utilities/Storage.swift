//
//  Storage.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

class Storage {

    fileprivate init() { }

    enum Directory {
        /** Only documents and other data that is user-generated, or that cannot otherwise be recreated
         by your application, should be stored in the `<Application_Home>/Documents` directory and will be
         automatically backed up by iCloud.
         */
        case documents

        /**
         Data that can be downloaded again or regenerated should be stored
         in the `<Application_Home>/Library/Caches` directory.

         Examples of files you should put in the Caches directory include database cache files
         and downloadable content, such as that used by magazine, newspaper, and map applications.
         */
        case caches
    }

    /// Returns URL constructed from specified directory
    ///
    /// - Parameter directory: directory to get URL.
    static private func getUrl(for directory: Directory) -> URL {
        var searchPathDirectory: FileManager.SearchPathDirectory

        switch directory {
        case .documents:
            searchPathDirectory = .documentDirectory
        case .caches:
            searchPathDirectory = .cachesDirectory
        }

        if let url = FileManager.default.urls(for: searchPathDirectory, in: .userDomainMask).first {
            return url
        } else {
            print("Could not create URL for specified directory!")
            preconditionFailure()
        }
    }

    /// Store an encodable struct to the specified directory on disk
    ///
    /// - Parameters:
    ///   - object: the encodable struct to store
    ///   - directory: where to store the struct
    ///   - filename: what to name the file where the struct data will be stored
    static func store<T: Encodable>(_ object: T, to directory: Directory, as filename: String) {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        print("Storing data in path: \(url.absoluteString)")

        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
                print("File exists at path: \(url.absoluteString) \nRemoving item.\n")
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            print(error.localizedDescription)
        }
    }

    /// Store a `Data` object to the specified directory on disk.
    ///
    /// - Parameters:
    ///   - data: the `Data` object to store.
    ///   - directory: where to struct the `Data`
    ///   - filename: what to name the file where `Data` will be stored.
    @discardableResult static func storeData(data: Data, to directory: Directory, as filename: String) -> Bool {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        print("Storing data in path: \(url.absoluteString)")
        do {
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
                print("File exists at path: \(url.absoluteString) \nRemoving item.\n")
            }
            return FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            print(error.localizedDescription)
            return false
        }
    }

    /// Retrieve and convert a struct from a file on disk
    ///
    /// - Parameters:
    ///   - filename: name of the file where struct data is stored
    ///   - directory: directory where struct data is stored
    ///   - type: struct type (i.e. Message.self)
    /// - Returns: decoded struct model(s) of data
    static func retrieve<T: Decodable>(_ filename: String, from directory: Directory, as type: T.Type) -> T? {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)

        if !FileManager.default.fileExists(atPath: url.path) {
            print("File at path \(url.path) does not exist!")
        }

        if let data = FileManager.default.contents(atPath: url.path) {
            let decoder = JSONDecoder()
            do {
                let model = try decoder.decode(type, from: data)
                return model
            } catch {
                print("\(error)")
                print(error.localizedDescription)
                return nil
            }
        } else {
            print("No data at \(url.path)!")
            return nil
        }
    }

    /// Retrieve `Data` from a file on disk.
    ///
    /// - Parameters:
    ///   - filename: name of the file where data is stored.
    ///   - directory: directory where data is stored.
    /// - Returns: The store `Data`, it is not found, it will return nil.
    static func retrieveData(filename: String, from directory: Directory) -> Data? {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)

        guard FileManager.default.fileExists(atPath: url.path) else {
            print("File at path \(url.path) does not exist!")
            return nil
        }

        guard let data = FileManager.default.contents(atPath: url.path) else {
            print("No data at \(url.path)!")
            return nil
        }

        return data
    }

    static func retrieveUrl(forFilename filename: String, from directory: Directory) -> URL? {
        guard fileExists(filename, in: directory) else { return nil }

        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        return url
    }

    /// Remove all files at specified directory
    ///
    /// - Parameter directory: directory to clear.
    static func clear(_ directory: Directory) {
        let url = getUrl(for: directory)
        do {
            let contents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [])
            for fileUrl in contents {
                try FileManager.default.removeItem(at: fileUrl)
            }
        } catch {
            print(error.localizedDescription)
        }
    }

    /// Remove specified file from specified directory
    ///
    /// - Parameters:
    ///   - filename: name of the file to remove.
    ///   - directory: directory where file is stored.
    /// - Returns: `true` is file was removed.
    @discardableResult
    static func remove(_ filename: String, from directory: Directory) -> Bool {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        if FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.removeItem(at: url)
                return true
            } catch {
                print(error.localizedDescription)
                return false
            }
        }

        return false
    }

    /// Verify if a file with the same name has been already stored.
    ///
    /// - Parameters:
    ///   - filename: name of the file to validate.
    ///   - directory: directory where file is stored.
    /// - Returns: Returns `Bool` indicating whether file exists at specified directory with specified file name.
    static func fileExists(_ filename: String, in directory: Directory) -> Bool {
        let url = getUrl(for: directory).appendingPathComponent(filename, isDirectory: false)
        return FileManager.default.fileExists(atPath: url.path)
    }
}
