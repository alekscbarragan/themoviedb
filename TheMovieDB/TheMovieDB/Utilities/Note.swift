//
//  Note.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol Note {
    var caseName: String { get }
    func post()
}

extension Note where Self: RawRepresentable, Self.RawValue == String {
    var caseName: String {
        return rawValue
    }
}

extension Note {
    var notificationName: String {
        return "\(type(of: self)).\(caseName)"
    }
    
    func post() {
        if Thread.isMainThread {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: self.notificationName), object: nil, userInfo: nil)
        } else {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: self.notificationName), object: nil, userInfo: nil)
            }
        }
    }
    
    func observe(observer: Any, selector: Selector) {
        stopObserving(observer: observer)
        NotificationCenter.default.addObserver(observer, selector: selector, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }

    func stopObserving(observer: Any) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
}
