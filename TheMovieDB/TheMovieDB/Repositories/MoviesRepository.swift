//
//  MoviesRepository.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

class MoviesRepository<Model: Equatable>: Repository {
    
    var items: [[Model]] {
        return _items
    }
    
    var sections: Int {
        return items.count
    }
    
    private var _items = [[Model]]()
    
    func add(item: Model, inSection section: Int = 0) {
        if items.isEmpty {
            print("inserting new section")
            _items.append([item])
        } else if items.count > section {
            print("inserting in existing section: \(section)")
            _items[section].append(item)
        } else {
            print("inserting a new section")
            _items.append([item])
        }
        
        print("sections:", sections)
    }
    
    func item(at indexPath: IndexPath) -> Model? {
        return items[indexPath.section][indexPath.row]
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        if items.isEmpty {
            return 0
        }
        return items[section].count
    }
    
    func update(item: Model) {
        guard let row = indexOf(item: item) else { return }
        // TODO: Safe insert
        _items[0][row] = item
    }
    
    func update(newItems: [Model], inSection section: Int = 0) {
        if items.isEmpty {
            _items.append(newItems)
        } else {
            if items.count > section {
                _items[section] = newItems
            }
        }
    }
    
    func remove(item: Model) {
        guard let row = indexOf(item: item) else { return }
        // TODO: Safe remove
        _items[0].remove(at: row)
    }
    
    func indexOf(item: Model) -> Int? {
        guard let items = self.items.first else { return nil }

        let index = items.firstIndex { (model) -> Bool in
            let found = model == item
            return found
        }
        return index
    }
    
}
