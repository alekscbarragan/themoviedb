//
//  ArrangedScrollView.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class ArrangedScrollView: UIScrollView {
    
    let stackView: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.axis = .vertical
        stack.spacing = 0
        return stack
    }()

    var insets: UIEdgeInsets = .zero {
        didSet {
            stackView.layoutMargins = insets
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.edgesToSuperview()
        stackView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        alwaysBounceVertical = true
        stackView.isLayoutMarginsRelativeArrangement = true
//        clipsToBounds = false
        isScrollEnabled = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Add all the given UIView instances as arranged subviews
    ///
    /// - Parameter subviews: All the UIView instances to be added as subviews
    func addArranged(subviews: UIView...) {
        subviews.forEach(stackView.addArrangedSubview)
    }
    
    func addArranged(subviews: [UIView]) {
        subviews.forEach(stackView.addArrangedSubview)
    }

    func didUpdateLayout() {
        contentSize = stackView.frame.size
    }
}
