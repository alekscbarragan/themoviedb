//
//  OperationObserver.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

protocol OperationObserver {
    func operationDidFinish(operation: Operation)
    func operation(operation: Operation, didFinishWithErrors errors: [Error])
}
