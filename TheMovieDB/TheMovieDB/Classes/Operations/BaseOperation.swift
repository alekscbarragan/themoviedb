//
//  BaseOperation.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 06/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import Foundation

class BaseOperation: Operation {
    enum State {
        case executing
        case finished
        case finishedWithErrors
        case awaiting
    }
    
    var errors: [Error] = []
    var observers: [OperationObserver] = []
    
    var state: State = .awaiting {
        didSet {
            willChangeValue(forKey: "isFinished")
            switch state {
            case .finished:
                for observer in observers {
                    observer.operationDidFinish(operation: self)
                }
                
            case .finishedWithErrors:
                for observer in observers {
                    observer.operation(operation: self, didFinishWithErrors: errors)
                }
                
            default:
                break
            }
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        switch state {
        case .finished, .finishedWithErrors:
            return true
        default:
            return false
        }
    }
    
    override func main() {
        state = .executing
        execute()
    }
    
    func execute() {
        fatalError("Override execute on `ZomatingOperation` subclasses.")
    }
    
    func finish() {
        state = .finished
    }
    
    func finish(with errors: [Error]) {
        self.errors = errors
        state = .finishedWithErrors
    }
    
    func add(observer: OperationObserver) {
        observers.append(observer)
    }
}
