//
//  Label.swift
//  TheMovieDB
//
//  Created by Alejandro Cárdenas on 07/10/19.
//  Copyright © 2019 Alejandro Cárdenas. All rights reserved.
//

import UIKit

final class Label: UILabel {
    let insets: UIEdgeInsets
    init(insets: UIEdgeInsets = .zero, text: String = "", heightConstant: CGFloat = 0) {
        self.insets = insets
        super.init(frame: .zero)
        self.text = text
        defaultConfiguration()
        if heightConstant > 0 {
            heightAnchor.constraint(equalToConstant: heightConstant).isActive = true
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: insets))
    }
}
