# TheMovieDB #

## Requirements ##

- Swift 5
- Xcode 11.2 Beta (11B41) 
- Cocoapods 1.8.3
- MacOS Catalina 10.15 Beta (19A582a) or above.

## Run project ##

Use `bundler` as gems manager.

* If you don't have `bundler` install run the following command:
```
$ gem install bundler
```

* Install gems
```
$ bundle install
```
* Go to project directory
```
$ cd TheMovieDB
```
*  Install pods
```
$ bundle exec pod install --repo-update
```

After pods has been installed, open `TheMovieDB.xcworkspace`

### Pending tasks ###
 * [ ] Favorite data persistance
 * [ ] Ignore favorite duplicate movies
 * [ ] Bookmarks (watchlist) data persistance
 * [ ] Ignore bookmarked duplicate movies

## Navigation ##
#### App navigation ####
![Navigation](Resources/TheMovieDB-Navigation.png)

#### Network client operations ####
![Navigation](Resources/TheMovieDB-NetworkOperations.png)

### Classes ###

* `AppNavigation`:
    - Handles main navigation of the app and shared instances such as coordinators and network clients. Serves a initial start and initial dependency injection of shared instances. 

* `NetworkClient`:
    - Protocol that define API network calls. It can be used to mock network calls for unit testing.

* `TheMovieDBNetworkClient`:
    - Release/debug implementaton of `NetworkClient` protocol.
    
* `Repository`:
    - Defines a repository.

* `MoviesRepository`:
    - Conforms and implements `Repository` protocol. Provides functionality for classes that depend on `IndexPath` such asa `UITableView` and `UICollectionView`

* `Coordinator`:
    - Protocol to implement a coordinator.

* `PopularMoviesCoordinator`:
    - Conforms and defines `Coordinator` protocol for the popular movies navigation flow. 

* `TopRatedMoviesCoordinator`:
    - Conforms and defines `Coordinator` protocol for top rated movies navigation flow.

* `UpcomingMoviesCoordinator`:
    - Confroms and defines `Coordinator` protocol for upcoming movies navigation flow. 

* `CenterViewController`:
    - It will handle the main content view of the app.

* `ContainerViewController`:
    - It handles logic between side view controller (burger menu) and `CenterViewController`

* `WatchListViewController`:
    - Show favorite and watch list movies that are added in popular, top rated and upcoming movies list.

* `MoviesViewController`:
    - Provides functionality to render collections of movies. 

* `MoviesView`:
    - Provies UI elements for `MoviesViewController`. However, it is not dependent on it. It can be used with other `UIViewController` subclasses. 

* `SearchViewController`:
    - A subclass of `MoviesViewController`. It adds functionality to work as a search online but uses `MoviesViewController` base to render searches. 

* `TopRatedMoviesViewController`:
    - A subclass of `MoviesViewController`. It uses `MoviesViewController` to render movies and provides its funcionality to request top rated movies network request.

* `UpcomingMoviesViewController`:
    - A subclass of `MoviesViewController`. It uses `MoviesViewController` to render movies and only proves its title and specific API to request upcoming movies.

* `MovieDetailViewController`
    - Renders details of a selected `Movie`. 

* `MovieDetailView`
    - A subclass of `UIView`. Provies UI elements to render a `Movie` in `MovieDetailViewController`. As `MoviesView`, it does not depend on its `view controller` to be used. It can be reused in others `UIViewController` instances. It relies on `delegation pattern` for communication to its view controller

* `GalleryViewController`:
    - `UIViewController` subclass that provides a video gallery of a `Movie`, if available. 

* `VideoCollectionViewCell`:
    - Cell to be used in `GalleryViewController`. 

* `MovieVide`:
    - Model to hold video data for a `Movie` instance.

* `ArrangedScrollView`:
    - A `UIScrollView` subclass that uses internally a `UIStackView` instance to stack UI elements and scrollable, if needed.

* `Label`:
    - A `UILabel` subclass enables injection of initial text and content insets upon creation.

* `Movie`:
    - Data `model` to represent a `Movie` data. Main model to be used withint a `MovieRepository` to render in `MoviesViewController`

* `Storage`:
    - Provides utility methods to read from disk from `documents` or `cache` directories. It proves a way to encode and decode models that conforms to `Codable` protocol. 

* `Note`:
    - Protocol that provies an easy way to add observers to `NotificationCenter` and post `Notification`s.

* `Notifications`:
    - Enum that implements `Note` protocol. 

* `Reusable`:
    - Protocol that enable an easy way to provide strings with the name of the class or class that conforms it. 

* `MovieResponse`:
    - Struct to parse and model initial response from themoviedb api.

* `MovieVideosResponse`:
    - Struct to parse and model inital response for consulting available videos of movies.

* `BaseOperation`:
    - A `Operation` subclass (NSOperation) and provides base implementation to be used in the app. It provides a way to observe operations.

* `BaseNetworkOperation`:
    - A `BaseOperation` subclass that provides functionality to make data tasks with `URLSession` and decodes responses for models that conforms `Decodable` protocol.

* `PopularMoviesOperation`:
    - A `BaseNetworkOperation` subclass that provides data to request popular movies from the themoviedb API.

* `UpcomingMoviesOperation`:
    - A `BaseNetworkOperation` subclass that provides data to request upcoming movies from the themoviedb API.

* `TopRatedMoviesOperation`:
    - A `BaseNetworkOperation` subclass that provides data to request top rated movies from the themoviedb API.

* `SearchMovieOperation`:
    - A `BaseNetworkOperation` subclass that provides data to query movies from the theMovieDB API.

* `MovieVideosOperation`:
    - A `BaseNetworkOperation` subclass that provides data to request videos data for a movie.

* `NullEndpoint`:
    - A struct to provide an empty endpoint. It's not meant to be used in production but for development.

* `PopularMoviesEndpoint`, `TopRatedMoviesEndpoint`, `UpcomingMoviesEndpoint`, `SearchMovieEndpoint`, `MovieVideosEndpoint`:
    - A struct that provies enough data for the endpoint to request popular movies.

* `EndpointConvertible`:
    - Protocol that defines which data an endpoint needs to provide to be use.

### Single responsability ###
The single responsability principle establishes that a class should only handle an specific task or only a single responsability. All the services that it needs, they need to be aligned with its responsability. And the way I see it, if a class it's going to use other instances to itself, they should be injected and the class should not know its implementation but only used it for what it is needed. 

### Personal opinion on good code or clean code ###
Good code or clean code, personally, I think it needs to be consistent, organized and readable. If a code is cosistent, if new programmers get into the project, once they familiarized with the code base, it'd be easy for them to search and find what they are looking for since the code its consistent. Being organized, personally, it is to organized files by its classes they declared and not throw everything in a single file. Code is meant to be read and not written. That is one of  my favorite sayings. If programmer do this, they essentially will be following, implicitly, best practices. These practices may be different depending on the organization or part of the world they are in.

## Screenshots ##

### Light mode ###
![light-mode](Resources/light-0.png) ![light-mode](Resources/light-1.png) ![light-mode](Resources/light-2.png)
![light-mode](Resources/light-3.png) ![light-mode](Resources/light-4.png) ![light-mode](Resources/light-5.png)
![light-mode](Resources/light-6.png) ![light-mode](Resources/light-8.png) ![light-mode](Resources/light-8.png) 

### Dark mode ###
![dark-mode](Resources/dark-0.png) ![dark-mode](Resources/dark-1.png) ![dark-mode](Resources/dark-3.png)
![dark-mode](Resources/dark-4.png) ![dark-mode](Resources/dark-5.png) ![dark-mode](Resources/dark-6.png)
![dark-mode](Resources/dark-7.png) ![dark-mode](Resources/dark-8.png) ![dark-mode](Resources/dark-8.png)
![dark-mode](Resources/dark-9.png)
